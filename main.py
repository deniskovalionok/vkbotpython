import vk_api
from vk_api.longpoll import VkLongPoll, VkEventType
from datetime import datetime
import random

session = vk_api.VkApi(token='674779100aee94ec2c9d3cd16565a8912cfd873125fdc12d14952349c317afdf7bbd6eab6b6ede8f3d31d')
session_api = session.get_api()
longpoll = VkLongPoll (session)

while True:
    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW:
            response = event.text
            if event.from_user and not event.from_me:
                if response == 'Играть':
                    session.method('messages.send', {'user_id': event.user_id, 'message': 'Программа выберет два числа и запишет их в первую и во вторую позицию, нужно угадать, какое число больше. Напишите 1 или 2\n', 'random_id':0})
                    a = random.randint(0, 10)
                    b = random.randint(0, 10)
                    f = str(a) + ' и ' + str(b)
                if response == '1' or response == '2':
                    c = int(response)
                    if (a > b):
                        d = 1
                    else:
                        d = 2
                    if (c == d):
                        e = "Ты выйграл! "
                    else:
                        e = "Ты проиграл! "
                    session.method('messages.send', {'user_id': event.user_id, 'message': e + " Это были цифры: " + f, 'random_id': 0})
